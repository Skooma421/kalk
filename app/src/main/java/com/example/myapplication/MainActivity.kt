package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.autofill.OnClickAction
import android.view.View
import android.widget.TextView
import org.w3c.dom.Text
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.resultTextView)}
    fun  numberClick(clickedView: View){
        if (clickedView is TextView) {
            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }


            resultTextView.text = result + number
        }
    }

    fun operationClick(clickedView : View) {
        if (clickedView is TextView) {
            val result = resultTextView.text.toString()
            if (result.isNotEmpty()) {
                this.operand = result.toDouble()
            }
            this.operation = clickedView.text.toString()
            resultTextView.text = ""
        }
    }

    fun deleteClick(view: android.view.View) {
        var shedegi = resultTextView.text.toString()
        val text = resultTextView.text.toString()
        if (text.isNotEmpty()) {
            resultTextView.text = text.dropLast(1)}}
    fun equalsClick(clickedView: View) {
        if (clickedView is TextView){
            val secondOperandText = resultTextView.text.toString()
            var secondOperand: Double=0.0

            if (secondOperandText.isNotEmpty()){
                secondOperand = secondOperandText.toDouble()
            }
            when(operation){
                "+"->resultTextView.text = (operand + secondOperand).toString()
                "-"->resultTextView.text = (operand - secondOperand).toString()
                "*"->resultTextView.text = (operand * secondOperand).toString()
                "/"->resultTextView.text = (operand / secondOperand).toString()
                "%"->resultTextView.text = (operand * secondOperand / 100).toString()
                "AC"->resultTextView.text = ""
                "DEL" ->  resultTextView.text = resultTextView.text.drop(1)
                else -> println("error")
            }


        }
    }
}